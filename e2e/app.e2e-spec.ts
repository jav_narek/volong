import { VoloPage } from './app.po';

describe('volo App', () => {
  let page: VoloPage;

  beforeEach(() => {
    page = new VoloPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

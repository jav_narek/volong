import { UsersRequestsService } from '../_services/index';
import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';

@Injectable()
export class EditGuard implements CanActivate {

    constructor(private router:Router, private usersRequestsService:UsersRequestsService) { }

    canActivate() {
        if (this.usersRequestsService.editableUser) {
            return true;
        }

        this.router.navigate(['dashboard/home']);
        return false;
    }
}
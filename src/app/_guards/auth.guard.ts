import { AuthService } from '../_services/index';
import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService:AuthService) { }

    canActivate() {
        if (this.authService.token) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}
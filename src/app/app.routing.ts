import { DashboardComponent } from './dashboard/dashboard.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard,EditGuard } from './_guards/index';


const dashboard: Routes=[
    { path: '', redirectTo: 'dashboard/home', pathMatch:'full' },
    { path: 'home', component: HomeComponent },
    { path: 'edit', component: EditUserComponent, canActivate:[EditGuard]}
]

const appRoutes: Routes = [
     { path: '', redirectTo: 'dashboard/home',pathMatch:'full'},
    { path: 'login', component: LoginComponent },
    { path: 'dashboard', component:DashboardComponent, canActivate:[AuthGuard], children:dashboard}
];



export const routing = RouterModule.forRoot(appRoutes);
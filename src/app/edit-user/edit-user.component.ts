import { User } from './../_model/user';
import { UsersRequestsService,AlertMessageService} from './../_services/index';
import { Component, OnInit } from '@angular/core';
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
        } from "@angular/router";

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editingUser:User;
  constructor(private alertMessageService:AlertMessageService,
              private router:Router,
              private requestService:UsersRequestsService) {
          this.editingUser=this.requestService.editableUser;
   }

   saveChanges(){
     this.requestService.updateUser(this.requestService.editableFirstName,this.requestService.editableLastName,this.editingUser.id)
     .subscribe(result=>{
       this.alertMessageService.alertMessage("User "+this.editingUser.first_name+" "+this.editingUser.last_name+" updated !","success");
       this.requestService.editableUser.first_name=this.requestService.editableFirstName;
       this.requestService.editableUser.last_name=this.requestService.editableLastName;
      }, err=>this.alertMessageService.alertMessage("User "+this.editingUser.first_name+" "+this.editingUser.last_name+" not updated !","error"));
     
     this.router.navigate(["dashboard/home"]);
   }

   cancelChanges(){
    this.router.navigate(["dashboard/home"]);
   }
  
  ngOnInit() {
     
  }

}

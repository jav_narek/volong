import { User } from './../_model/user';
import { Injectable } from '@angular/core';

declare var $:any;
@Injectable()
export class AlertMessageService {

    message={
        text:'',
        type:''
    };
    

    alertMessage(text:string, type:string){
        $("#alertModal").modal('show');
        this.message.text=text;
        this.message.type=type;
        setTimeout(()=>$("#alertModal").modal('hide'), 2000);
           
    }
}

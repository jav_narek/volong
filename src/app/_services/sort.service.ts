import { Injectable } from '@angular/core';

@Injectable()
export class SortingService {
    sortedUsers;
    returnedValue=-1;
    constructor() {}

     sortByProp(sortUsers, prop, returnedValue){
           
            this.sortedUsers=sortUsers.sort(function (a, b) {
            if (a[prop] > b[prop]) {
                return returnedValue;
            }
            if (a[prop] < b[prop]) {
                return -(returnedValue);
            }
            return 0;
            });
        this.returnedValue*=-1;
    }
}
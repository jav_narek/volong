import { GlobalVariable } from './../_globals/globals';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {
    token:string;
    url:string=GlobalVariable.BASE_API_URL;
    constructor(private http: Http) { 
        this.token=localStorage.getItem('currentUserToken');
    }

    takeToken(email: string, password: string) {
        return this.http.post(`${this.url}/login`, ({ email: email, password: password }) )
            .map((response: Response) => { 
                this.token = response.json().token;
                if(this.token){
                    localStorage.setItem('currentUserToken', response.json().token);
                }
                return this.token;
            });
    }

    removeToken(){
         localStorage.removeItem('currentUserToken');
    }
}
import { GlobalVariable } from './../_globals/globals';
import { User } from './../_model/user';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class UsersRequestsService {
  users:User[];
  lastUserId:number;
  editableUser:User;
  editableFirstName:string;
  editableLastName:string;
  url:string=GlobalVariable.BASE_API_URL;
  constructor(private http:Http) { }

  getUsers(pageNumber:number) {
      return this.http.get(`${this.url}/users?page=${pageNumber}`).map((response: Response) => response.json());
  }
  
  getPages(){
     return this.http.get(`${this.url}/users?page=1`).map((response: Response) => response.json().total_pages);
  }

  deleteUser(id:number){
        return this.http.delete(`${this.url}/users/${id}`).map((response: Response) => response.json());
  }

  addUser(name: string, surname: string){
     return this.http.post(`${this.url}/users`, ({ name: name, surname: surname })).map((response: Response) => response.json());
  }

  updateUser(name: string, surname: string, id:number){
    return this.http.post(`${this.url}/users/${id}`, ({ name: name, surname: surname })).map((response: Response) => response.json());
  }

lastUserIdentifire(){
        let userId:number[]=[0];
       for (let user of this.users){
          userId.push(user.id);
       }
        this.lastUserId=Math.max.apply(null, userId);
    }

}

import { AlertMessageService } from './../_services/index';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';

declare var $:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private routing:Router, private alertMessageService:AlertMessageService) { }
  message=this.alertMessageService.message; 
  ngOnInit() {
    this.routing.navigate(["dashboard/home"]);
  }

  ngOnDestroy(){
    this.alertMessageService.message={text:'',type:''};
  }

}


import { User } from './../_model/user';
import { LoginComponent } from './../login/login.component';
import { AuthService,UsersRequestsService,SortingService,AlertMessageService} from './../_services/index';
import { Component, OnInit } from '@angular/core';
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
        } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, Validators} from "@angular/forms";

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    currentIndex:number;
    currentUser:object={};
    newName:string="";
    newSurname:string="";
    addForm:FormGroup;
    
    constructor(private router:Router,
                private authService:AuthService,
                private requestService:UsersRequestsService,
                private sortingService:SortingService , 
                private alertMessageService:AlertMessageService) { 
                        this.addForm = new FormGroup({
                                "userName": new FormControl("", Validators.required),
                                "userLastname": new FormControl("", Validators.required)
            });
    }

      loadUser(id){
        this.requestService.getUsers(id).subscribe(users => this.requestService.users = users.data, 
            err=>this.alertMessageService.alertMessage("Users not loadeded !","error"));     
    }


    private deleteUser(ind, user){
        $('#deleteModal').modal('hide');
        this.requestService.deleteUser(user.id)
        .subscribe(result=>{
                        this.alertMessageService.alertMessage("User "+user.first_name+" "+user.last_name+" deleted !","success");
                        this.requestService.users.splice(ind,1);
                        }, err=>this.alertMessageService.alertMessage("User "+user.first_name+" "+user.last_name+" not deleted !","error"));
    }

    confirmDelete(ind,user){
        this.currentIndex=ind;
        this.currentUser=user;
        $('#deleteModal').modal('show');
    }

    deleteModal(visibility){
        $('#deleteModal').modal(visibility);
    }

    addModal(visibility){
        $('#addModal').modal(visibility);
    }

    
    confirmAddNewUser(name,surname){
        $('#addModal').modal('hide');
        this.requestService.addUser(name,surname)
        .subscribe(result=>{this.alertMessageService.alertMessage("User "+name+" "+surname+" added !","success");
                                                                this.createNewUser(name,surname)},
                                                                err=>this.alertMessageService.alertMessage("User "+name+" "+surname+" not added !","error"));
    }
    
    createNewUser(name,surname){
        this.requestService.lastUserIdentifire();
        let lastUser;
        if(this.requestService.users.length===0){
            lastUser=0;
        }else{
            lastUser=this.requestService.lastUserId;
        }
        let newUser:User={
            "id": lastUser+1,
            "first_name": name,
            "last_name": surname,
            "avatar": "http://lorempixel.com/128/128"
        }
        this.requestService.users.push(newUser);
        this.newName=name;
        this.newSurname=surname;
    }

    sort(byProp){
        this.sortingService.sortByProp(this.requestService.users, byProp, this.sortingService.returnedValue);
        this.requestService.users=this.sortingService.sortedUsers;
    }

    editUser(user){
        this.requestService.editableFirstName=user.first_name;
        this.requestService.editableLastName=user.last_name;
        this.requestService.editableUser=user;
        this.router.navigate(["dashboard/edit"]);

    }

    ngOnInit() {
       if(!this.requestService.users){
          this.loadUser(1);
         }
         
    }
}

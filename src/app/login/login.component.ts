import { AuthService,UsersRequestsService } from '../_services/index';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
    CanActivate,
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from "@angular/router";
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  email:string;
  password:string;
  
  constructor( private router:Router, private authService:AuthService, private requestService:UsersRequestsService) { }

  login() {
            this.authService.takeToken(this.email, this.password)
           .subscribe((result)=>this.router.navigate(["dashboard/home"]),
                   error=>console.error("UNSUCCESSFUL"))
            
    }
  ngOnInit() {
    if (localStorage.getItem('currentUserToken')) {
            this.router.navigate(["dashboard/home"]);
        }
  }

}

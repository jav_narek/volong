import { UsersRequestsService, AuthService} from './../_services/index';
import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Router} from "@angular/router";
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private router:Router, private requestService:UsersRequestsService, private authService:AuthService) { }
  logout() {
        this.router.navigate(["login"]);
        this.requestService.users=null;
        this.authService.removeToken();
       }
  ngOnInit() {
  }

}

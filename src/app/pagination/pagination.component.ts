import { Component, OnInit,EventEmitter, Input, Output } from '@angular/core';
import { UsersRequestsService } from "app/_services/index";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  paginationArr:number[];
  constructor( private requestService:UsersRequestsService) { }

  @Output() onLoad= new EventEmitter<number>();

  ngOnInit() {
    this.pages();
  }

  load(id){
      this.onLoad.emit(id);
  }


  pageArr(page){
        this.paginationArr=[];
        for(let i=0; i<page; i++){
            this.paginationArr.push(i);
        }  
    }

    pages(){
            this.requestService.getPages().subscribe(page=>{this.pageArr(page)}, err=>(console.log("error")));
        }

}

import { AuthService, UsersRequestsService, SortingService, AlertMessageService} from './_services/index';
import {routing} from './app.routing'
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule,provideRoutes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard, EditGuard } from './_guards/index';
import {EmailValidatorDirective, PassValidatorDirective} from './_validators/index';
import {FirstLetterCapitalize} from './_pipes/flcap.pipe';
import { EditUserComponent } from './edit-user/edit-user.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PaginationComponent } from './pagination/pagination.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FirstLetterCapitalize,
    EmailValidatorDirective,
    PassValidatorDirective,
    EditUserComponent,
    NavBarComponent,
    DashboardComponent,
    PaginationComponent
  ],
  providers: [
    AuthGuard,
    EditGuard,
    AuthService,
    UsersRequestsService,
    SortingService,
    AlertMessageService,
    EmailValidatorDirective,
    PassValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from "@angular/forms";

@Directive({
    selector: '[email-validator]',
    providers: [{provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}]
  })
  export class EmailValidatorDirective implements Validator {
    rexp:RegExp=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
    validate(c: AbstractControl):ValidationErrors {
      return this.rexp.test(c.value)?null:{'WrongEmail': {value: c.value}};
    }
  }


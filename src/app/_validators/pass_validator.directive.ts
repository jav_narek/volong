import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from "@angular/forms";

@Directive({
    selector: '[pass-validator]',
    providers: [{provide: NG_VALIDATORS, useExisting: PassValidatorDirective, multi: true}]
  })
  export class PassValidatorDirective implements Validator {
    rexp:RegExp=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*]{8,}$/
    
    validate(c: AbstractControl):ValidationErrors {
      return this.rexp.test(c.value)?null:{'WrongPassword': {value: c.value}};
    }
  }


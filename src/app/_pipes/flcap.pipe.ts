import {Pipe,PipeTransform} from "@angular/core";

@Pipe({
    name: 'flcap'
})
export class FirstLetterCapitalize implements PipeTransform {

    transform(value:any) {
        if (value) {
            value=value.toLowerCase();
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
        return value;
    }

}